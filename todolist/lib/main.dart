import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'task.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ToDoList(),
    );
  }
}

class ToDoList extends StatefulWidget {
  @override
  State createState() => _ToDoList();
}

class _ToDoList extends State {
  List<Task> tasks = [
    Task(
        taskName: "First Task", description: "first task", date: "14-Feb-2024"),
    Task(
        taskName: "Second Task",
        description: "Second task",
        date: "14-Feb-2024"),
    Task(
        taskName: "Third Task", description: "Third task", date: "14-Feb-2024"),
    Task(
        taskName: "Fourth Task",
        description: "Fourth task",
        date: "14-Feb-2024"),
  ];

  List<Color?> colors = [
    HexColor("#FAE8E8"),
    HexColor("#E8EDFA"),
    HexColor("#FAF9E8"),
    HexColor("#FAE8E8")
  ];

  TextEditingController tasknameController = TextEditingController();
  TextEditingController desnameController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  bool delete = false;
  bool isEdit = false;

  void showErrorBottomSheetModalSubmit() {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40), topRight: Radius.circular(40))),
        context: context,
        builder: (BuildContext context) {
          return SizedBox(
            height: 200,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Center(
                    child: Text("Please fill the fields",
                        style: GoogleFonts.quicksand(
                            fontSize: 28, fontWeight: FontWeight.w600))),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(
                      HexColor("#008B94"),
                    ),
                    fixedSize: const MaterialStatePropertyAll(
                      Size(175, 20),
                    ),
                  ),
                  child: const Text(
                    "OK",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  void addValues() {
    setState(() {
      if (tasknameController.text != "" &&
          desnameController.text != "" &&
          dateController.text != "") {
        tasks.add(Task(
            date: dateController.text,
            taskName: tasknameController.text,
            description: desnameController.text));

        tasknameController.text = "";
        desnameController.text = "";
        dateController.text = "";
      }
    });
  }

  void deleteTask(int index) {
    tasks.removeAt(index);
    setState(() {});
  }

  void addEditedValues(int index) {
    setState(() {
      tasks[index].taskName = tasknameController.text;

      tasks[index].date = dateController.text;

      tasks[index].description = desnameController.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    print(colors.length);
    return Scaffold(
      appBar: AppBar(
        elevation: 10,
        toolbarHeight: 70,
        title: Text(
          "To Do List",
          style:
              GoogleFonts.quicksand(fontSize: 26, fontWeight: FontWeight.w700),
        ),
        backgroundColor: HexColor('#02A7B1'),
      ),
      body: ListView.separated(
        itemBuilder: (context, index) {
          return Container(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
            margin: const EdgeInsets.all(15),
            // height: 130,
            width: 90,
            decoration: BoxDecoration(
              boxShadow: const [
                BoxShadow(
                    offset: Offset(3, 3), color: Colors.black, blurRadius: 15)
              ],
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              // color: HexColor("#FAE8E8"),
              color: colors[index % colors.length],
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      height: 42,
                      width: 42,
                      margin: const EdgeInsets.fromLTRB(10, 10, 25, 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        // color: HexColor('#FFFFFF'),
                        color: HexColor("#FAE8E8"),
                      ),

                      // child: Image.asset(
                      //   '/assets/task2.jpg',
                      //   height: 5,
                      //   width: 5,
                      // ),

                      child: Image.network(
                        "https://i.pinimg.com/736x/6e/bf/6f/6ebf6f1eea0f9e8245e1f4a941af2e64.jpg",
                        height: 3,
                        width: 3,
                        // fit: BoxFit.fill,
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            tasks[index].taskName!,
                            style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w600, fontSize: 20),
                          ),
                          const SizedBox(height: 10),
                          Text(
                            tasks[index].description!,
                            style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w500, fontSize: 15),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(tasks[index].date!),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            isEdit = true;
                            if (isEdit) {
                              tasknameController.text = tasks[index].taskName!;
                              desnameController.text =
                                  tasks[index].description!;
                              dateController.text = tasks[index].date!;
                            }
                            showModalBottomSheet(
                              isScrollControlled: true,
                              shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(40),
                                      topRight: Radius.circular(40))),
                              context: context,
                              builder: (BuildContext context) => Container(
                                margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                padding: MediaQuery.of(context).viewInsets,
                                child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const SizedBox(
                                        height: 15,
                                      ),
                                      Text(
                                        "Edit Task",
                                        style: GoogleFonts.quicksand(
                                            fontSize: 28,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      const SizedBox(
                                        height: 15,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Title",
                                            style: GoogleFonts.quicksand(
                                                fontSize: 13.75,
                                                fontWeight: FontWeight.w400,
                                                color: HexColor("#008B94")),
                                          ),
                                          TextField(
                                            scrollPadding:
                                                const EdgeInsets.all(5),
                                            controller: tasknameController,
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  const EdgeInsets.all(20),
                                              enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: HexColor("#008B94"),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 15,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Description",
                                            style: GoogleFonts.quicksand(
                                                fontSize: 13.75,
                                                fontWeight: FontWeight.w400,
                                                color: HexColor("#008B94")),
                                          ),
                                          TextField(
                                            spellCheckConfiguration:
                                                const SpellCheckConfiguration(),
                                            controller: desnameController,
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  const EdgeInsets.all(20),
                                              enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: HexColor("#008B94"),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 15,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Date",
                                            style: GoogleFonts.quicksand(
                                                fontSize: 13.75,
                                                fontWeight: FontWeight.w400,
                                                color: HexColor("#008B94")),
                                          ),
                                          TextField(
                                            onTap: () async {
                                              DateTime? pickedDate =
                                                  await showDatePicker(
                                                      context: context,
                                                      initialDate:
                                                          DateTime.now(),
                                                      firstDate: DateTime(2024),
                                                      lastDate: DateTime(2025));

                                              String format = DateFormat.yMMMd()
                                                  .format(pickedDate!);

                                              setState(() {
                                                dateController.text = format;
                                              });
                                            },
                                            readOnly: true,
                                            scrollPadding:
                                                const EdgeInsets.all(5),
                                            controller: dateController,
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  const EdgeInsets.all(20),
                                              enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: HexColor("#008B94"),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 25,
                                      ),
                                      ElevatedButton(
                                        style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStatePropertyAll(
                                                    HexColor("#008B94"))),
                                        onPressed: () {
                                          // if (tasknameController.text != "" ||
                                          //     desnameController.text != "" ||
                                          //     dateController.text != "") {
                                          Navigator.of(context).pop();
                                          addEditedValues(index);
                                          // } else {
                                          //   showErrorBottomSheetModalEdit();
                                          // }
                                        },
                                        child: Container(
                                          height: 60,
                                          width: 300,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: const Center(
                                            child: Text(
                                              "Submit",
                                              style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 25,
                                      ),
                                    ]),
                              ),
                            );
                          },
                          child: Icon(
                            Icons.edit,
                            color: HexColor('#02A7B1'),
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        GestureDetector(
                          child: Icon(
                            Icons.delete,
                            color: HexColor('#02A7B1'),
                          ),
                          onTap: () {
                            showModalBottomSheet(
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(40),
                                    topRight: Radius.circular(40),
                                  ),
                                ),
                                context: context,
                                builder: (BuildContext context) {
                                  return SizedBox(
                                      child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const SizedBox(
                                        height: 25,
                                      ),
                                      Text(
                                        "Are you sure ?",
                                        style: GoogleFonts.quicksand(
                                            fontSize: 28,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          ElevatedButton(
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStatePropertyAll(
                                                HexColor("#008B94"),
                                              ),
                                              fixedSize:
                                                  const MaterialStatePropertyAll(
                                                Size(175, 20),
                                              ),
                                            ),
                                            onPressed: () {
                                              delete = true;
                                              deleteTask(index);
                                              Navigator.of(context).pop();
                                            },
                                            child: const Text(
                                              "Yes",
                                              style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ),
                                          ElevatedButton(
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStatePropertyAll(
                                                HexColor("#008B94"),
                                              ),
                                              fixedSize:
                                                  const MaterialStatePropertyAll(
                                                Size(175, 20),
                                              ),
                                            ),
                                            onPressed: () {
                                              setState(() {
                                                Navigator.of(context).pop();
                                              });
                                            },
                                            child: const Text(
                                              "No",
                                              style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ));
                                });
                          },
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
          );
        },
        separatorBuilder: (context, index) => const SizedBox(),
        itemCount: tasks.length,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          tasknameController.text = "";
          desnameController.text = "";
          dateController.text = "";

          showModalBottomSheet(
            isScrollControlled: true,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40))),
            context: context,
            builder: (BuildContext context) => Container(
              // padding: const EdgeInsets.fromLTRB(20, 0, 20, 15),
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              padding: MediaQuery.of(context).viewInsets,
              child: Column(
                  // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Create Task",
                      style: GoogleFonts.quicksand(
                          fontSize: 28, fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Title",
                          style: GoogleFonts.quicksand(
                              fontSize: 13.75,
                              fontWeight: FontWeight.w400,
                              color: HexColor("#008B94")),
                        ),
                        TextField(
                          scrollPadding: const EdgeInsets.all(5),
                          controller: tasknameController,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(20),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: HexColor("#008B94"),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Description",
                          style: GoogleFonts.quicksand(
                              fontSize: 13.75,
                              fontWeight: FontWeight.w400,
                              color: HexColor("#008B94")),
                        ),
                        TextField(
                          spellCheckConfiguration:
                              const SpellCheckConfiguration(),
                          controller: desnameController,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(20),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: HexColor("#008B94"),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Date",
                          style: GoogleFonts.quicksand(
                              fontSize: 13.75,
                              fontWeight: FontWeight.w400,
                              color: HexColor("#008B94")),
                        ),
                        TextField(
                          onTap: () async {
                            DateTime? pickedDate = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(2024),
                                lastDate: DateTime(2025));

                            String format =
                                DateFormat.yMMMd().format(pickedDate!);

                            setState(() {
                              dateController.text = format;
                            });
                          },
                          readOnly: true,
                          scrollPadding: const EdgeInsets.all(5),
                          controller: dateController,
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.calendar_month,
                                color: HexColor("#008B94")),
                            contentPadding: const EdgeInsets.all(20),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: HexColor("#008B94"),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(HexColor("#008B94"))),
                      onPressed: () {
                        if (tasknameController.text != "" &&
                            desnameController.text != "" &&
                            dateController.text != "") {
                          Navigator.of(context).pop();
                          addValues();
                        } else {
                          showErrorBottomSheetModalSubmit();
                        }
                      },
                      child: Container(
                        height: 60,
                        width: 300,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: const Center(
                          child: Text(
                            "Submit",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                  ]),
            ),
          );
        },
        backgroundColor: HexColor('#02A7B1'),
        child: const Icon(Icons.add),
      ),
    );
  }
}
