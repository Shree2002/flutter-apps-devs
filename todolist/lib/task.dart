import 'package:flutter/material.dart';

class Task {
  String? taskName;
  String? description;
  String? date;
  int? id;

  Task({this.taskName, this.description, this.date});
}
