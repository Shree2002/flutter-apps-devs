import "package:sqflite/sqflite.dart";
import "package:path/path.dart";

class Painting {
  int? id;
  String paintingName;
  String painter;
  String paintingDecription;
  String category;
  int price;

  Painting(
      {required this.paintingName,
      required this.painter,
      required this.category,
      required this.paintingDecription,
      required this.price,
      this.id});

  Map<String, dynamic> paintingMap() {
    return {
      "paintingName": paintingName,
      "painter": painter,
      "paintingDescription": paintingDecription,
      "category": category,
      "price": price
    };
  }

  @override
  String toString() {
    return "id:$id,paintingName:$paintingName,painter:$painter,painting Description:$paintingDecription,category:$category,price:$price";
  }
}

dynamic database;

dynamic getDatabase() async {
  return openDatabase(
    join(await getDatabasesPath(), "PaintingDB2.db"),
    version: 1,
    onCreate: (db, version) {
      db.execute('''
          create table Painting(
            paintingName text,
            painter text,
            paintingDescription text,
            category text,
            price int,
            id int primary key 
          )
''');
    },
  );
}

Future<void> addPainting(Painting p) async {
  final localDB = await getDatabase();

  await localDB.insert("Painting", p.paintingMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
  print("Data Added");
  print(await getPaintings());
}

Future<List<Painting>> getPaintings() async {
  final localDB = await getDatabase();

  List<Map<String, dynamic>> pm = await localDB.query("Painting");
  print("Sent data");
  return List.generate(pm.length, (index) {
    return Painting(
        id: pm[index]["id"],
        paintingName: pm[index]["paintingName"],
        paintingDecription: pm[index]["paintingDescription"],
        painter: pm[index]["painter"],
        price: pm[index]["price"],
        category: pm[index]["category"]);
  });
}

// Future<void> deleteAll() async {
//   final localDB = await getDatabase();

//   localDB.delete("Painting");
// }
