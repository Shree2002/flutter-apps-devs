import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";
import "package:hexcolor/hexcolor.dart";

class PersonalScreen extends StatefulWidget {
  const PersonalScreen({super.key});

  @override
  State createState() => _PersonalScreenState();
}

class _PersonalScreenState extends State {
  TextEditingController firstName = TextEditingController();
  TextEditingController lastname = TextEditingController();
  TextEditingController phoneNo = TextEditingController();
  TextEditingController email = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          margin: const EdgeInsets.only(left: 20, top: 40),
          child: Text(
            "Personal Information",
            style: GoogleFonts.jost(
              fontSize: 22,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        margin: const EdgeInsets.only(
          top: 40,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset("assets/user profile.png"),
              Container(
                height: 76,
                margin: const EdgeInsets.only(bottom: 8, top: 30),
                width: 340,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: HexColor("#E0E0E0"),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 7, top: 7),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "First Name",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          color: HexColor("#727272"),
                        ),
                      ),
                      TextFormField(
                        controller: firstName,
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                height: 76,
                margin: const EdgeInsets.only(bottom: 8, top: 10),
                width: 340,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: HexColor("#E0E0E0"),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 7, top: 7),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Last Name",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          color: HexColor("#727272"),
                        ),
                      ),
                      TextFormField(
                        controller: lastname,
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                height: 76,
                margin: const EdgeInsets.only(bottom: 8, top: 10),
                width: 340,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: HexColor("#E0E0E0"),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 7, top: 7),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Phone Number",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          color: HexColor("#727272"),
                        ),
                      ),
                      TextFormField(
                        controller: phoneNo,
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                        keyboardType: TextInputType.number,
                      )
                    ],
                  ),
                ),
              ),
              Container(
                height: 76,
                margin: const EdgeInsets.only(bottom: 8, top: 10),
                width: 340,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: HexColor("#E0E0E0"),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 7, top: 7),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Email",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          color: HexColor("#727272"),
                        ),
                      ),
                      TextFormField(
                        controller: email,
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                        keyboardType: TextInputType.emailAddress,
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 50),
                child: ElevatedButton(
                  onPressed: () {},
                  style: ButtonStyle(
                    fixedSize: const MaterialStatePropertyAll(Size(300, 50)),
                    backgroundColor:
                        const MaterialStatePropertyAll(Colors.black),
                    shape: MaterialStateProperty.all(
                      const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.zero),
                      ),
                    ),
                  ),
                  child: Text(
                    "Save Changes",
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AccountInfo extends StatefulWidget {
  @override
  State createState() => _AccountInfo();
}

class _AccountInfo extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            height: 50,
          ),
          const Text(
            "Profile",
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Image.asset("assets/user profile.png"),
          const SizedBox(
            height: 20,
          ),
          const Text(
            "Kartik Charkupalli",
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const PersonalScreen()));
            },
            child: Container(
              margin: const EdgeInsets.only(left: 30, top: 80, right: 30),
              child: const Row(
                children: [
                  Icon(
                    Icons.person_2,
                    color: Colors.orange,
                  ),
                  SizedBox(
                    width: 25,
                  ),
                  Text(
                    "Personal Information",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Spacer(),
                  Icon(
                    Icons.next_plan,
                    color: Colors.black,
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 30, top: 30, right: 30),
            child: const Row(
              children: [
                Icon(
                  Icons.image,
                  color: Colors.orange,
                ),
                SizedBox(
                  width: 25,
                ),
                Text(
                  "Painting Collections",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Spacer(),
                Icon(
                  Icons.next_plan,
                  color: Colors.black,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 30, top: 30, right: 30),
            child: const Row(
              children: [
                Icon(
                  Icons.settings,
                  color: Colors.orange,
                ),
                SizedBox(
                  width: 25,
                ),
                Text(
                  "Painting Collections",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Spacer(),
                Icon(
                  Icons.next_plan,
                  color: Colors.black,
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 250,
          ),
          Container(
            height: 50,
            width: 330,
            margin: const EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.red,
              ),
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.logout,
                  color: Colors.red,
                ),
                Text(
                  "Logout Account",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.red,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
