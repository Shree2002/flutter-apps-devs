import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'paintingAddtion.dart';

class DataDisplay extends StatefulWidget {
  const DataDisplay({super.key});
  @override
  State createState() => DataDisplay1();
}

class DataDisplay1 extends State {
  List<Painting> render = [];
  bool flag = true;

  void setList() async {
    List<Painting> temp = await getPaintings();
    render = temp;
  }

  @override
  Widget build(BuildContext context) {
    setList();
    if (flag) {
      setState(() {});
      flag = false;
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Data Display",
          style: GoogleFonts.poppins(fontSize: 20, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: render.length,
        itemBuilder: (context, index) {
          return Container(
            // height: 125,
            // width: 222,
            margin: const EdgeInsets.fromLTRB(15, 20, 15, 20),
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(50),
                    topLeft: Radius.circular(50)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    blurRadius: 25,
                    offset: Offset(5, 5),
                  )
                ]),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text("Painting Name  :  ${render[index].paintingName}",
                    style: GoogleFonts.poppins(
                        fontSize: 17, fontWeight: FontWeight.w500)),
                Text("Painter  :  ${render[index].painter}",
                    style: GoogleFonts.poppins(
                        fontSize: 17, fontWeight: FontWeight.w500)),
                Text("Description  :  ${render[index].paintingDecription}",
                    style: GoogleFonts.poppins(
                        fontSize: 17, fontWeight: FontWeight.w500)),
                Text("Price : ${render[index].price.toString()}",
                    style: GoogleFonts.poppins(
                        fontSize: 17, fontWeight: FontWeight.w500)),
              ],
            ),
          );
        },
      ),
    );
  }
}
