import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import "package:hexcolor/hexcolor.dart";
import 'package:dotted_border/dotted_border.dart';
import 'paintingAddtion.dart';
import 'paintingDataDisplay.dart';

class AddArt extends StatefulWidget {
  @override
  State createState() => _addart();
}

class _addart extends State {
  TextEditingController paintingName = TextEditingController();
  TextEditingController paintingDescription = TextEditingController();
  TextEditingController category = TextEditingController();
  TextEditingController paintingPrice = TextEditingController();
  TextEditingController painter = TextEditingController();

  List<Painting> listofpaintings = [];

  Future<void> listData() async {
    List<Painting> al = await getPaintings();

    listofpaintings = al;
  }

  List<Painting> render = [];
  bool flag = true;

  void setList() async {
    render = await getPaintings();
    print(render);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Add Art",
          style: GoogleFonts.poppins(fontSize: 20, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(36, 15, 36, 0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 76,
                margin: const EdgeInsets.only(bottom: 8),
                width: 340,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: HexColor("#E0E0E0"),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 7, top: 7),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Painting Name",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          color: HexColor("#727272"),
                        ),
                      ),
                      TextFormField(
                        controller: paintingName,
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 8),
                height: 76,
                width: 340,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: HexColor("#E0E0E0"),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 7, top: 7),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Painter",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          color: HexColor("#727272"),
                        ),
                      ),
                      TextFormField(
                        controller: painter,
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 8),
                height: 76,
                width: 340,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: HexColor("#E0E0E0"),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 7, top: 7),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Painting Description",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          color: HexColor("#727272"),
                        ),
                      ),
                      TextFormField(
                        controller: paintingDescription,
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 8),
                height: 76,
                width: 340,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: HexColor("#E0E0E0"),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 7, top: 7),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Category",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          color: HexColor("#727272"),
                        ),
                      ),
                      TextFormField(
                        controller: category,
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 20),
                height: 76,
                width: 340,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: HexColor("#E0E0E0"),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 7, top: 7),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Price",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          color: HexColor("#727272"),
                        ),
                      ),
                      TextFormField(
                        controller: paintingPrice,
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                      )
                    ],
                  ),
                ),
              ),
              DottedBorder(
                child: SizedBox(
                  height: 165,
                  width: 182,
                  child: Column(
                    children: [
                      Image.asset(
                        "assets/upload1.png",
                        height: 110,
                      ),
                      Text("Upload Image",
                          style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                          ))
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 25),
                child: ElevatedButton(
                  onPressed: () {
                    if (painter.text != "" &&
                        paintingDescription.text != "" &&
                        category.text != "" &&
                        paintingPrice.text != "" &&
                        paintingName.text != "") {
                      addPainting(
                        Painting(
                            paintingName: paintingName.text,
                            painter: painter.text,
                            category: category.text,
                            paintingDecription: paintingDescription.text,
                            price: int.parse(paintingPrice.text)),
                      );
                    }
                    print(render);
                    setState(() {});
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return const DataDisplay();
                    }));

                    painter.text = "";
                    paintingDescription.text = "";
                    paintingName.text = "";
                    paintingPrice.text = "";
                    category.text = "";
                  },
                  style: ButtonStyle(
                    fixedSize: const MaterialStatePropertyAll(Size(290, 54)),
                    backgroundColor:
                        const MaterialStatePropertyAll(Colors.black),
                    shape: MaterialStateProperty.all(
                      const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.zero),
                      ),
                    ),
                  ),
                  child: Text(
                    "Submit Art Auction",
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
