
int count = 1;

class User {
  final String userName;
  final String password;
  final int? id;

  const User(
      {required this.userName, required this.password,  this.id});

  Map<String, dynamic> userMap() {
    return {"userName": userName, "password": password, "id": id};
  }

  @override
  String toString() {
    return "UserName : $userName, Password : $password, id : $id ";
  }
}

