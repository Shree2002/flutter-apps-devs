import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";

class Notification1 extends StatefulWidget {
  @override
  State createState() => _notification();
}

class _notification extends State {
  List<Map> list = [
    {"Name": "Shree Added new painting", "date": "18-3-2024"},
    {"Name": "Yash Added new painting", "date": "18-3-2024"},
    {"Name": "Manas Added new painting", "date": "18-3-2024"},
    {"Name": "Mukesh Added new painting", "date": "18-3-2024"},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Notifications",
          style: GoogleFonts.poppins(fontSize: 20, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return Container(
            height: 70,
            width: 200,
            margin: const EdgeInsets.fromLTRB(15, 10, 10, 15),
            decoration: const BoxDecoration(
              // gradient: LinearGradient(colors: [Colors.blue, Colors.white]),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black, offset: Offset(5, 5), blurRadius: 10)
              ],
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(32),
                topRight: Radius.circular(32),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(list[index]["Name"],
                    style: GoogleFonts.poppins(
                        fontSize: 17, fontWeight: FontWeight.w700)),
                Text(list[index]["date"],
                    style: GoogleFonts.poppins(
                        fontSize: 13, fontWeight: FontWeight.w500)),
              ],
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
