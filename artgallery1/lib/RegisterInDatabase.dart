import 'user_data.dart';
import "package:path/path.dart";
import "package:sqflite/sqflite.dart";

dynamic database1;

dynamic getDatabase1() async {
  return openDatabase(
    join(await getDatabasesPath(), "PaintingDB3.db"),
    version: 1,
    onCreate: (db, version) {
      db.execute('''
          create table User(
            userName text,
            password text,
            id int primary key 
          )
''');
    },
  );
}

Future<void> addUser(User p) async {
  final localDB = await getDatabase1();

  await localDB.insert("User", p.userMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
  print("User Data Added");
}

Future<List<User>> getUserList() async {
  final localDB = await getDatabase1();

  List<Map<String, dynamic>> pm = await localDB.query("User");
  print("Sent data");
  return List.generate(
    pm.length,
    (index) {
      return User(
        id: pm[index]["id"],
        userName: pm[index]["userName"],
        password: pm[index]["password"],
      );
    },
  );
}

Future<bool> getUser(String userName, String password) async {
  List<User> mp = await getUserList();

  for (int i = 0; i < mp.length; i++) {
    if (mp[i].userName == userName && mp[i].password == password) {
      return true;
    }
  }
  
  return false;
}
