import 'package:artgallery1/RegisterInDatabase.dart';
import 'package:flutter/material.dart';
import "package:hexcolor/hexcolor.dart";
import 'package:google_fonts/google_fonts.dart';
import 'user_data.dart';
import 'add_art.dart';
import 'home_artist_details.dart';
import 'buy_now.dart';
import "account_info.dart";
import 'notification.dart';

class ArtApp extends StatefulWidget {
  const ArtApp({super.key});

  @override
  State createState() => _ArtAppState();
}

class _ArtAppState extends State {
  int c = 0;
  final TextEditingController _nameRegisterController = TextEditingController();
  final TextEditingController _passRegisterController = TextEditingController();
  final TextEditingController _confRegisterController = TextEditingController();
  final TextEditingController _nameSignInController = TextEditingController();
  final TextEditingController _passSignInController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String enteredPassword = "";
  bool isPasswordVisible = false;
  bool isConfPasswordVisible = false;
  bool passwordsMatch = false;

  List<User> userList = [
    const User(userName: "kartik", password: "k123"),
    const User(userName: "onkar", password: "o123"),
  ];

  Scaffold getStartedScreen() {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 300,
              width: 300,
              child: Image.asset(
                "assets/logo-1.png",
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  c++;
                });
              },
              child: Container(
                height: 40,
                width: 200,
                padding: const EdgeInsets.only(left: 50, top: 7),
                color: Colors.black,
                child: Text(
                  "Get Started",
                  style: GoogleFonts.jost(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Scaffold getOverview1Screen() {
    return Scaffold(
      body: SizedBox(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("assets/Illustration.png"),
            const SizedBox(
              height: 100,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  c++;
                });
              },
              child: Container(
                height: 40,
                width: 200,
                padding: const EdgeInsets.only(left: 75, top: 5),
                color: Colors.black,
                child: Text(
                  "Next",
                  style: GoogleFonts.jost(
                    fontSize: 22,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Scaffold getOverview2Screen() {
    return Scaffold(
      body: SizedBox(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("assets/cuate.png"),
            const SizedBox(
              height: 100,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  c++;
                });
              },
              child: Container(
                height: 40,
                width: 200,
                padding: const EdgeInsets.only(left: 75, top: 5),
                color: Colors.black,
                child: Text(
                  "Next",
                  style: GoogleFonts.jost(
                    fontSize: 22,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Scaffold getOverview3Screen() {
    return Scaffold(
      body: SizedBox(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("assets/bro.png"),
            const SizedBox(
              height: 100,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  c++;
                });
              },
              child: Container(
                height: 40,
                width: 200,
                padding: const EdgeInsets.only(left: 75, top: 5),
                color: Colors.black,
                child: Text(
                  "Start",
                  style: GoogleFonts.jost(
                    fontSize: 22,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Scaffold getRegisterOrSignInScreen() {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.only(top: 50),
              child: Text(
                "Welcome to",
                style: GoogleFonts.jost(
                  fontSize: 40,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(
              height: 200,
              width: 200,
              child: Image.asset(
                "assets/logo-1.png",
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  c++;
                });
              },
              child: Container(
                height: 40,
                width: 200,
                padding: const EdgeInsets.only(left: 65, top: 5),
                color: Colors.black,
                child: Text(
                  "Register",
                  style: GoogleFonts.jost(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  // _validateAndProceed();
                  print("BEFORE SIGIN BUTTON:$c");

                  c += 2;

                  print("SIGIN BUTTON:$c");
                });
              },
              child: Container(
                height: 40,
                width: 200,
                padding: const EdgeInsets.only(left: 65, top: 5),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                  ),
                ),
                child: Text(
                  "Sign In",
                  style: GoogleFonts.jost(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              "Or",
              style: GoogleFonts.jost(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),
            const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.facebook,
                  color: Colors.blue,
                  size: 40,
                ),
                SizedBox(
                  width: 25,
                ),
                Icon(
                  Icons.youtube_searched_for,
                  color: Colors.blue,
                  size: 40,
                ),
                SizedBox(
                  width: 25,
                ),
                Icon(
                  Icons.apple,
                  color: Colors.black,
                  size: 40,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void _validateAndProceed() {
    print("In validate and proceed");
    print("In ValidateAndProceed ${_formKey.currentState!.validate()}");
    if (_formKey.currentState!.validate()) {
      if (passwordsMatch) {
        setState(
          () {
            // userList.add(User(
            //     userName: _nameRegisterController.text,
            //     password: _passRegisterController.text));

            addUser(User(
                userName: _nameRegisterController.text,
                password: _passRegisterController.text));

            c += 2;

            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text("User Successfully Registered"),
                backgroundColor: Colors.green,
                duration: Duration(seconds: 5),
                showCloseIcon: true,
                behavior: SnackBarBehavior.floating,
                margin: EdgeInsets.all(5),
              ),
            );
          },
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Password does not match"),
            backgroundColor: Colors.red,
            duration: Duration(seconds: 5),
            showCloseIcon: true,
            behavior: SnackBarBehavior.floating,
            margin: EdgeInsets.all(5),
          ),
        );
      }
    }
  }

  String? _validatePassword(String value) {
    if (value.isEmpty) {
      return "Please Confirm Password";
    } else if (value != _passRegisterController.text) {
      setState(() {
        passwordsMatch = false;
      });
      return "Passwords do not match";
    } else {
      // Passwords match
      setState(() {
        passwordsMatch = true;
      });

      return null;
    }
  }

  Scaffold getRegisterScreen() {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.only(top: 100, left: 40),
              child: Text(
                "It's a pleasure to meet you!",
                style: GoogleFonts.jost(
                  fontSize: 25,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(top: 10, left: 40),
              child: Text(
                "Sign up and get started",
                style: GoogleFonts.jost(
                  fontSize: 20,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
            Container(
              // height: 50,
              width: 300,
              margin: const EdgeInsets.only(top: 30, left: 40),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: TextFormField(
                controller: _nameRegisterController,
                decoration: const InputDecoration(
                  hintText: "Please enter your username",
                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(
                      color: Colors.black,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(
                      color: Colors.black,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Please Enter Username";
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.name,
              ),
            ),
            Container(
              // height: 50,
              width: 300,
              margin: const EdgeInsets.only(top: 20, left: 40),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: TextFormField(
                controller: _passRegisterController,
                obscureText: !isPasswordVisible,
                obscuringCharacter: "*",
                decoration: InputDecoration(
                  hintText: "Please enter your password",
                  contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                  border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(
                      color: Colors.black,
                    ),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  errorBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                  ),
                  suffixIcon: GestureDetector(
                    onTap: () {
                      setState(() {
                        isPasswordVisible = !isPasswordVisible;
                      });
                    },
                    child: Icon(
                      isPasswordVisible
                          ? Icons.visibility_off
                          : Icons.remove_red_eye_outlined,
                    ),
                  ),
                ),
                validator: (value) {
                  print("Value In PasswordVisible ${value!}");
                  // _validatePassword(value);
                  if (value.isEmpty) {
                    return "Please Enter Password";
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            Container(
              // height: 50,
              width: 300,
              margin: const EdgeInsets.only(top: 20, left: 40),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: TextFormField(
                controller: _confRegisterController,
                obscureText: !isConfPasswordVisible,
                obscuringCharacter: "*",
                decoration: InputDecoration(
                  hintText: "Confirm your password",
                  // contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                  contentPadding: const EdgeInsets.only(left: 10),
                  border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(
                      color: Colors.black,
                    ),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  errorBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                  ),
                  suffixIcon: GestureDetector(
                    onTap: () {
                      setState(() {
                        isConfPasswordVisible = !isConfPasswordVisible;
                      });
                    },
                    child: Icon(
                      isConfPasswordVisible
                          ? Icons.visibility_off
                          : Icons.remove_red_eye_outlined,
                    ),
                  ),
                ),
                validator: (value) {
                  print("Value In ConfPasswordVisible ${value!}");
                  return _validatePassword(value);
                },
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _validateAndProceed();
                });
              },
              child: Container(
                height: 40,
                width: 200,
                margin: const EdgeInsets.only(top: 50, left: 40),
                padding: const EdgeInsets.only(left: 65, top: 5),
                color: Colors.black,
                child: Text(
                  "Register",
                  style: GoogleFonts.jost(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _validateNextScreen() {
    bool loginValidated = _formKey.currentState!.validate();
    print("IN VALIDATENEXT SCREEN");
    // print("In Validate NExtScreen ${_formKey.currentState!.validate()}");
    // bool loginValidated = _formKey.currentState!.validate();

    if (loginValidated) {
      String enteredUserName = _nameSignInController.text;
      enteredPassword = _passSignInController.text;

      bool matchingUser = userList.any(
        (element) =>
            element.userName == enteredUserName &&
            element.password == enteredPassword,
      );

      if (matchingUser) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Login Successful"),
            backgroundColor: Colors.green,
            duration: Duration(seconds: 1),
          ),
        );
        setState(() {
          c++;
        });
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Invalid Username or Password"),
            backgroundColor: Colors.red,
            duration: Duration(seconds: 5),
            showCloseIcon: true,
            behavior: SnackBarBehavior.floating,
            margin: EdgeInsets.all(5),
          ),
        );
      }
    }
  }

  bool isPasswordSignInVisible = false;

  Scaffold getSignInScreen() {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.only(top: 100, left: 40),
              child: Text(
                "It's great to have you back!",
                style: GoogleFonts.jost(
                  fontSize: 25,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(top: 10, left: 40),
              child: Text(
                "Sign up and continue your jounney",
                style: GoogleFonts.jost(
                  fontSize: 20,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
            Container(
              // height: 50,
              width: 300,
              margin: const EdgeInsets.only(top: 30, left: 40),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: TextFormField(
                controller: _nameSignInController,
                decoration: const InputDecoration(
                  hintText: "Please enter your username",
                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(
                      color: Colors.black,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(
                      color: Colors.red,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Please Enter Username";
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.name,
              ),
            ),
            Container(
              // height: 50,
              width: 300,
              margin: const EdgeInsets.only(top: 20, left: 40),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: TextFormField(
                controller: _passSignInController,
                obscureText: !isPasswordSignInVisible,
                obscuringCharacter: "*",
                decoration: InputDecoration(
                  hintText: "Please enter your password",
                  contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                  border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(
                      color: Colors.black,
                    ),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  errorBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                  ),
                  suffixIcon: GestureDetector(
                    onTap: () {
                      setState(() {
                        isPasswordSignInVisible = !isPasswordSignInVisible;
                      });
                    },
                    child: Icon(
                      isPasswordSignInVisible
                          ? Icons.visibility_off
                          : Icons.remove_red_eye_outlined,
                    ),
                  ),
                ),
                validator: (value) {
                  print("In PasswordSignInVisible $value!");
                  // return _validatePassword(value!);    // should not be written
                  if (value == null || value.isEmpty) {
                    return "Please Enter Password";
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _validateNextScreen();
                });
              },
              child: Container(
                height: 40,
                width: 200,
                margin: const EdgeInsets.only(top: 50, left: 40),
                padding: const EdgeInsets.only(left: 65, top: 5),
                color: Colors.black,
                child: Text(
                  "Sign In",
                  style: GoogleFonts.jost(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  late int _selectedIndex;
  late List<Widget> widgetOptions;

  void initState() {
    super.initState();
    _selectedIndex = 0;

    widgetOptions = [
      const Center(child: Text("Home Page")),
      const Center(child: Text("Search Page")),
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            height: 50,
          ),
          const Text(
            "Profile",
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Image.asset("assets/user profile.png"),
          const SizedBox(
            height: 20,
          ),
          const Text(
            "Kartik Charkupalli",
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
          GestureDetector(
            onTap: () {
              _navigateToNextScreen(context);
            },
            child: Container(
              margin: const EdgeInsets.only(left: 30, top: 80, right: 30),
              child: const Row(
                children: [
                  Icon(
                    Icons.person_2,
                    color: Colors.orange,
                  ),
                  SizedBox(
                    width: 25,
                  ),
                  Text(
                    "Personal Information",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Spacer(),
                  Icon(
                    Icons.next_plan,
                    color: Colors.black,
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 30, top: 30, right: 30),
            child: const Row(
              children: [
                Icon(
                  Icons.image,
                  color: Colors.orange,
                ),
                SizedBox(
                  width: 25,
                ),
                Text(
                  "Painting Collections",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Spacer(),
                Icon(
                  Icons.next_plan,
                  color: Colors.black,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 30, top: 30, right: 30),
            child: const Row(
              children: [
                Icon(
                  Icons.settings,
                  color: Colors.orange,
                ),
                SizedBox(
                  width: 25,
                ),
                Text(
                  "Painting Collections",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Spacer(),
                Icon(
                  Icons.next_plan,
                  color: Colors.black,
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 250,
          ),
          Container(
            height: 50,
            width: 330,
            margin: const EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.red,
              ),
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.logout,
                  color: Colors.red,
                ),
                Text(
                  "Logout Account",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.red,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    ];
  }

  void _navigateToNextScreen(BuildContext context) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => const PersonalScreen()));
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  TextEditingController textcontrol = TextEditingController();

  Scaffold getMainBody() {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(18, 80, 18, 0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Image.asset("assets/user profile.png"),
                      const SizedBox(
                        width: 10,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Hello Kartik",
                            style: GoogleFonts.poppins(
                                fontSize: 20,
                                fontWeight: FontWeight.w500,
                                color: Colors.black),
                          ),
                          Text(
                            "Let's start buying",
                            style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: HexColor("#727272"),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => AddArt()));
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(20),
                          ),
                          border: Border.all(color: HexColor("#E0E0E0"))),
                      child: Icon(
                        Icons.add,
                        color: HexColor("#ED893E"),
                        size: 25,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: TextFormField(
                  controller: textcontrol,
                  decoration: InputDecoration(
                    prefixIcon: const Icon(Icons.search),
                    border: const OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    hintText: "Search Arts",
                    hintStyle: GoogleFonts.poppins(
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                      color: HexColor("#727272"),
                    ),
                  ),
                ),
              ),
              Text(
                "Popular Arts",
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: Colors.black),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    SizedBox(
                      height: 353,
                      width: 206,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 9,
                            ),
                            Image.asset("assets/first art.png"),
                            const SizedBox(
                              height: 9,
                            ),
                            Text(
                              "Unreveal Meaning",
                              style: GoogleFonts.poppins(
                                  fontSize: 14, fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Image.asset(
                                  "assets/user profile.png",
                                  height: 31,
                                  width: 31,
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Creator",
                                      style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 10,
                                          color: HexColor("#727272")),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ArtistDetails()));
                                      },
                                      child: Text(
                                        "Kartik Yamaha",
                                        style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 10,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Start from",
                                      style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 10,
                                          color: HexColor("#727272")),
                                    ),
                                    Text(
                                      "Rs.200.00",
                                      style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 10,
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const Buy_now()));
                                  },
                                  style: ButtonStyle(
                                    fixedSize: const MaterialStatePropertyAll(
                                        Size(187, 36)),
                                    backgroundColor:
                                        const MaterialStatePropertyAll(
                                            Colors.black),
                                    shape: MaterialStateProperty.all(
                                      const RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.all(Radius.zero),
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                    "Buy Art",
                                    style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      height: 353,
                      width: 206,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 9,
                            ),
                            Image.asset(
                              "assets/second art.jpg",
                              width: 205,
                              height: 207,
                            ),
                            const SizedBox(
                              height: 9,
                            ),
                            Text(
                              "Modern Art",
                              style: GoogleFonts.poppins(
                                  fontSize: 14, fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Image.asset(
                                  "assets/user profile.png",
                                  height: 31,
                                  width: 31,
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Creator",
                                      style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 10,
                                          color: HexColor("#727272")),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ArtistDetails()));
                                      },
                                      child: Text(
                                        "Kartik Yamaha",
                                        style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 10,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Start from",
                                      style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 10,
                                          color: HexColor("#727272")),
                                    ),
                                    Text(
                                      "Rs.200.00",
                                      style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 10,
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ElevatedButton(
                                  onPressed: () {},
                                  style: ButtonStyle(
                                    fixedSize: const MaterialStatePropertyAll(
                                        Size(187, 36)),
                                    backgroundColor:
                                        const MaterialStatePropertyAll(
                                            Colors.black),
                                    shape: MaterialStateProperty.all(
                                      const RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.all(Radius.zero),
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                    "Buy Art",
                                    style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 35,
              ),
              Text(
                "Trending Now",
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: Colors.black),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    SizedBox(
                      height: 353,
                      width: 206,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 9,
                            ),
                            Image.asset(
                              "assets/krishna.jpg",
                              height: 207,
                              width: 250,
                            ),
                            const SizedBox(
                              height: 9,
                            ),
                            Text(
                              "Krishna Painting",
                              style: GoogleFonts.poppins(
                                  fontSize: 14, fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Image.asset(
                                  "assets/user profile.png",
                                  height: 31,
                                  width: 31,
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Creator",
                                      style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 10,
                                          color: HexColor("#727272")),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ArtistDetails()));
                                      },
                                      child: Text(
                                        "Kartik Yamaha",
                                        style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 10,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Start from",
                                      style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 10,
                                          color: HexColor("#727272")),
                                    ),
                                    Text(
                                      "Rs.200.00",
                                      style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 10,
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ElevatedButton(
                                  onPressed: () {},
                                  style: ButtonStyle(
                                    fixedSize: const MaterialStatePropertyAll(
                                        Size(187, 36)),
                                    backgroundColor:
                                        const MaterialStatePropertyAll(
                                            Colors.black),
                                    shape: MaterialStateProperty.all(
                                      const RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.all(Radius.zero),
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                    "Buy Art",
                                    style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      height: 353,
                      width: 206,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 9,
                            ),
                            Image.asset(
                              "assets/art1.jpg",
                              width: 205,
                              height: 207,
                            ),
                            const SizedBox(
                              height: 9,
                            ),
                            Text(
                              "Painting Women",
                              style: GoogleFonts.poppins(
                                  fontSize: 14, fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Image.asset(
                                  "assets/user profile.png",
                                  height: 31,
                                  width: 31,
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Creator",
                                      style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 10,
                                          color: HexColor("#727272")),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ArtistDetails()));
                                      },
                                      child: Text(
                                        "Kartik Yamaha",
                                        style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 10,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Start from",
                                      style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 10,
                                          color: HexColor("#727272")),
                                    ),
                                    Text(
                                      "Rs.200.00",
                                      style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 10,
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ElevatedButton(
                                  onPressed: () {},
                                  style: ButtonStyle(
                                    fixedSize: const MaterialStatePropertyAll(
                                        Size(187, 36)),
                                    backgroundColor:
                                        const MaterialStatePropertyAll(
                                            Colors.black),
                                    shape: MaterialStateProperty.all(
                                      const RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.all(Radius.zero),
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                    "Buy Art",
                                    style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          const BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: IconButton(
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => Notification1()));
                },
                icon: const Icon(Icons.notifications)),
            label: "Notifications",
          ),
          BottomNavigationBarItem(
            icon: IconButton(
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => AccountInfo()));
                },
                icon: const Icon(Icons.account_circle)),
            label: "Account",
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.green,
        onTap: _onItemTapped,
        elevation: 5,
      ),
    );
  }

  Scaffold multipleScreens() {
    if (c == 0) {
      return getStartedScreen();
    } else if (c == 1) {
      return getOverview1Screen();
    } else if (c == 2) {
      return getOverview2Screen();
    } else if (c == 3) {
      return getOverview3Screen();
    } else if (c == 4) {
      return getRegisterOrSignInScreen();
    } else if (c == 5) {
      return getRegisterScreen();
    } else if (c == 6) {
      print("In sign in screen");
      return getSignInScreen();
    } else if (c == 7) {
      print("In home body");
      return getMainBody();
    } else {
      print("In else body");
      return Scaffold();
    }
  }

  @override
  Widget build(BuildContext context) {
    return multipleScreens();
  }
}

// class PersonalScreen extends StatefulWidget {
//   const PersonalScreen({super.key});

//   @override
//   State createState() => _PersonalScreenState();
// }

// class _PersonalScreenState extends State {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Container(
//           margin: const EdgeInsets.only(left: 20, top: 40),
//           child: Text(
//             "Personal Information",
//             style: GoogleFonts.jost(
//               fontSize: 22,
//               fontWeight: FontWeight.w600,
//             ),
//           ),
//         ),
//       ),
//       body: Container(
//         height: double.infinity,
//         width: double.infinity,
//         margin: const EdgeInsets.only(
//           top: 40,
//         ),
//         child: Column(
//           children: [
//             Image.asset("assets/user profile.png"),
//           ],
//         ),
//       ),
//     );
//   }
// }
