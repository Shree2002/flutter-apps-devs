import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";
import "package:hexcolor/hexcolor.dart";

class ArtistDetails extends StatefulWidget {
  @override
  State createState() => _artistdetails();
}

class _artistdetails extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Artist Details",
          style: GoogleFonts.poppins(fontSize: 20, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsetsDirectional.only(
          start: 34,
          end: 34,
          top: 24,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Image.asset("assets/user profile1.png"),
                Column(
                  children: [
                    Text("66",
                        style: GoogleFonts.poppins(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: HexColor("#ED893E"))),
                    Text("Arts",
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: HexColor("#727272")))
                  ],
                ),
                Column(
                  children: [
                    Text("Sold",
                        style: GoogleFonts.poppins(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: HexColor("#ED893E"))),
                    Text("Arts",
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: HexColor("#727272")))
                  ],
                ),
                Column(
                  children: [
                    Text("Rs 234.00",
                        style: GoogleFonts.poppins(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: HexColor("#ED893E"))),
                    Text("Average Price",
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: HexColor("#727272")))
                  ],
                )
              ],
            ),
            const SizedBox(height: 15),
            Text(
              "Kartik Yamaha",
              style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w500, fontSize: 20),
            ),
            const SizedBox(height: 15),
            Text(
              "I am constantly exploring new forms of expression and pushing the boundaries of what is possible. I am drawn to the beauty and complexity of the world around me, and I strive to capture this essence in my creations.",
              style: GoogleFonts.poppins(
                fontSize: 12,
                fontWeight: FontWeight.w500,
                color: HexColor("#727272"),
              ),
            ),
            const SizedBox(height: 15),
            Text("Paintings",
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500, fontSize: 20)),
            const SizedBox(height: 15),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  Container(
                    height: 194,
                    width: 151,
                    margin: const EdgeInsets.only(right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset("assets/firstartmain.png"),
                        Text(
                          "Unreveal Mean",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w400, fontSize: 15),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 194,
                    width: 151,
                    margin: const EdgeInsets.only(right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                          "assets/second art.jpg",
                          height: 152,
                          width: 151,
                        ),
                        Text(
                          "Modern Art",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w400, fontSize: 15),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  Container(
                    height: 194,
                    width: 151,
                    margin: const EdgeInsets.only(right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                          "assets/krishna.jpg",
                          height: 152,
                          width: 151,
                        ),
                        Text(
                          "Krishna Painting",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w400, fontSize: 15),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 194,
                    width: 151,
                    margin: const EdgeInsets.only(right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                          "assets/art1.jpg",
                          height: 152,
                          width: 151,
                        ),
                        Text(
                          "Women Painting",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w400, fontSize: 15),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
