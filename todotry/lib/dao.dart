import "package:path/path.dart";
import "package:sqflite/sqflite.dart";
import "package:flutter/widgets.dart";
import "task.dart";

dynamic database;

class dao {
  dynamic getDatabase() async {
    return await openDatabase(join(await getDatabasesPath(), "ToDoListDB.db"),
        version: 1, onCreate: (db, version) {
      db.execute('''
          create table TaskList(
               taskName text,
               description text,
               date text,
                id int primary key autoincrement,
                isDone int
               )
''');
    });
  }
}

// List<Task> tasks = [
//   Task(
//       id: count++,
//       taskName: "First Task",
//       description: "first task",
//       date: "14-Feb-2024",
//       isDone: 1),
//   Task(
//       id: count++,
//       taskName: "Second Task",
//       description: "Second task",
//       date: "14-Feb-2024",
//       isDone: 1),
//   Task(
//       id: count++,
//       taskName: "Third Task",
//       description: "Third task",
//       date: "14-Feb-2024",
//       isDone: 1),
//   Task(
//       id: count++,
//       taskName: "Fourth Task",
//       description: "Fourth task",
//       date: "14-Feb-2024",
//       isDone: 1),
// ];

Future<List<Task>> getData() async {
  final localDB = await dao().getDatabase();

  List<Map<String, dynamic>> ll = await localDB.query("TaskList");

  return List.generate(ll.length, (index) {
    return Task(
      id: ll[index]["id"],
      isDone: ll[index]["isDone"],
      description: ll[index]["description"],
      taskName: ll[index]["taskName"],
      date: ll[index]["date"],
    );
  });
}

Future<void> addTask(Task item) async {
  final localDB = await dao().getDatabase();

  await localDB.insert("TaskList", item.taskMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
}

Future<void> deleteTask(Task item) async {
  final localDB = await dao().getDatabase();

  await localDB.delete(
    "TaskList",
    where: "id=?",
    whereArgs: [item.id],
  );
}

Future<void> updateTask(Task item) async {
  final localDB = await dao().getDatabase();

  await localDB
      .update("TaskList", item.taskMap(), where: "id=?", whereArgs: [item.id]);
}

int count = 0;
