class Task {
  String? taskName;
  String? description;
  String? date;
  int? id; 
  int isDone;

  Task({this.taskName, this.description, this.date, required this.isDone, required this.id});

  Map<String, dynamic> taskMap() {
    return {
      "taskName": taskName,
      "description": description,
      "date": date,
      "id": id,
      "isDone": isDone
    };
  }

@override
  String toString() {
    return "Task id: $id,name: $taskName, description: $description, date: $date, id: $id, isDone: $isDone ";
  }
}
