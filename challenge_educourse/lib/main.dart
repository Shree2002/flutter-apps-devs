import 'package:flutter/material.dart';
import 'secondPage.dart';
import 'package:hexcolor/hexcolor.dart';
import "package:google_fonts/google_fonts.dart";

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Challenge(),
      // home: SecondPage(),
    );
  }
}

class Challenge extends StatefulWidget {
  const Challenge({super.key});
  @override
  State createState() => _Challenge();
}

class _Challenge extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor("#CDDADA"),
      // appBar: AppBar(
      //   toolbarHeight: 50,
      //   backgroundColor: HexColor("#CDDADA"),
      //   actions: const [
      //     Icon(
      //       Icons.notifications_none_outlined,
      //       size: 26,
      //     )
      //   ],
      //   leading: const Icon(
      //     Icons.menu,
      //     size: 26,
      //   ),
      // ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 10, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 45,
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(
                        Icons.menu,
                        size: 40,
                      ),
                      Icon(
                        Icons.notifications_none_outlined,
                        size: 35,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  Text(
                    "Welcome to new",
                    style: GoogleFonts.jost(
                        fontSize: 35, fontWeight: FontWeight.w300),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Educourse",
                    style: GoogleFonts.jost(
                        fontSize: 37, fontWeight: FontWeight.w700),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  TextField(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: const Icon(
                        Icons.search_outlined,
                        size: 27,
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(35),
                        borderSide: BorderSide.none,
                      ),
                      hintText: "Enter Your Keyword",
                    ),
                  ),
                ],
              )),
          const SizedBox(height: 20),
          Expanded(
            child: Container(
              width: double.infinity,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(38),
                  topRight: Radius.circular(38),
                ),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 15, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Course For You",
                      style: GoogleFonts.jost(
                          fontWeight: FontWeight.w500, fontSize: 18),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => SecondPage()));
                            },
                            child: Container(
                              height: 244,
                              width: 190,
                              padding: const EdgeInsets.all(18),
                              margin: const EdgeInsets.fromLTRB(0, 0, 15, 20),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                gradient: LinearGradient(colors: [
                                  HexColor("#C50462"),
                                  HexColor("#500370")
                                ], transform: const GradientRotation(20)),
                              ),
                              child: Column(
                                children: [
                                  Text(
                                    "UX Designer From Scratch",
                                    style: GoogleFonts.jost(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 17,
                                        color: Colors.white),
                                  ),
                                  Image.asset("assets/c1.png")
                                ],
                              ),
                            ),
                          ),
                          Container(
                              height: 242,
                              width: 190,
                              padding: const EdgeInsets.all(18),
                              margin: const EdgeInsets.fromLTRB(0, 0, 15, 20),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                gradient: LinearGradient(
                                  colors: [
                                    HexColor("#004DE4"),
                                    HexColor("#012F87")
                                  ],
                                  transform: const GradientRotation(20),
                                ),
                              ),
                              child: Column(
                                children: [
                                  Text(
                                    "Design Thinking as The Beginner",
                                    style: GoogleFonts.jost(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 17,
                                        color: Colors.white),
                                  ),
                                  Image.asset("assets/c2.png")
                                ],
                              ))
                        ],
                      ),
                    ),
                    Text(
                      "Course By Category",
                      style: GoogleFonts.jost(
                          fontWeight: FontWeight.w500, fontSize: 18),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        const SizedBox(
                          width: 18,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 42,
                              width: 42,
                              decoration: BoxDecoration(
                                color: HexColor("#190038"),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Image.asset(
                                "assets/i1.png",
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              "UI/UX",
                              style: GoogleFonts.jost(
                                  fontWeight: FontWeight.w400, fontSize: 14),
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 47,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 42,
                              width: 42,
                              decoration: BoxDecoration(
                                color: HexColor("#190038"),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Image.asset(
                                "assets/i2.png",
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              "VISUAL",
                              style: GoogleFonts.jost(
                                  fontWeight: FontWeight.w400, fontSize: 14),
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 42,
                              width: 42,
                              decoration: BoxDecoration(
                                color: HexColor("#190038"),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Image.asset(
                                "assets/i3.png",
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              "ILLUSTRATION",
                              style: GoogleFonts.jost(
                                  fontWeight: FontWeight.w400, fontSize: 14),
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 42,
                              width: 42,
                              decoration: BoxDecoration(
                                color: HexColor("#190038"),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Image.asset(
                                "assets/i4.png",
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              "PHOTO",
                              style: GoogleFonts.jost(
                                  fontWeight: FontWeight.w400, fontSize: 14),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
