import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import "package:google_fonts/google_fonts.dart";

class SecondPage extends StatelessWidget {
  List<Map> Lectures = [
    {
      "title": "Introduction",
      "d": "A Simple dummy text",
    },
    {
      "title": "Introduction",
      "d": "A Simple dummy text",
    },
    {
      "title": "Introduction",
      "d": "A Simple dummy text",
    },
    {
      "title": "Introduction",
      "d": "A Simple dummy text",
    },
    {
      "title": "Introduction",
      "d": "A Simple dummy text",
    },
    {
      "title": "Introduction",
      "d": "A Simple dummy text",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [HexColor("#C50462"), HexColor("#500370")],
            transform: const GradientRotation(20),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: const EdgeInsets.only(
                  left: 20,
                  top: 47,
                ),
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: const Icon(
                    size: 30,
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                )),
            const SizedBox(
              height: 10,
            ),
            Container(
                padding: EdgeInsets.only(left: 38, right: 38),
                child: Column(
                  children: [
                    Text(
                      "UX Designer From Scratch",
                      style: GoogleFonts.jost(
                          fontSize: 32.61,
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Basic guideline & tips & tricks for how to become a UX designer easily.",
                      style: GoogleFonts.jost(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: HexColor("#E4CDE1")),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Image.asset("assets/p2i1.png"),
                            const SizedBox(
                              width: 4,
                            ),
                            Text(
                              "Author:",
                              style: GoogleFonts.jost(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: HexColor("#BE9AC5"),
                              ),
                            ),
                            const SizedBox(
                              width: 4,
                            ),
                            Text(
                              "Jenny",
                              style: GoogleFonts.jost(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              "4.8",
                              style: GoogleFonts.jost(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Colors.white,
                              ),
                            ),
                            Image.asset("assets/Star.png"),
                            Text(
                              "(20 review)",
                              style: GoogleFonts.jost(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: HexColor("#BE9AC5"),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                )),
            const SizedBox(
              height: 25,
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(38),
                    topRight: Radius.circular(38),
                  ),
                ),
                child: ListView.builder(
                    itemCount: Lectures.length,
                    itemBuilder: (context, index) {
                      return Container(
                        margin:
                            EdgeInsets.only(left: 30, right: 30, bottom: 20),
                        width: 250,
                        height: 70,
                        decoration: const BoxDecoration(color: Colors.white
                            // color: Colors.white,
                            // boxShadow: [
                            //   BoxShadow(
                            //       color: Color.fromARGB(255, 207, 204, 204),
                            //       blurRadius: 50)
                            // ],
                            ),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                  left: 5, right: 10, top: 5, bottom: 5),
                              height: 60,
                              width: 46,
                              decoration: BoxDecoration(
                                  color: HexColor("#E6EFEF"),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12))),
                              child: Image.asset("assets/youtube.png"),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                  Lectures[index]["title"],
                                  style: GoogleFonts.jost(
                                      fontSize: 17,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  Lectures[index]["d"],
                                  style: GoogleFonts.jost(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      color: HexColor("#8F8F8F")),
                                )
                              ],
                            )
                          ],
                        ),
                      );
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
