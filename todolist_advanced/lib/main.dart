import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'task.dart';
import 'package:intl/intl.dart';
import 'loginForm.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        // home: todo(),
        home: todo());
  }
}

class todo extends StatefulWidget {
  @override
  State createState() => _todo();
}

class _todo extends State {
  List<Task> tasks = [
    Task(
        taskName: "First Task",
        description: "first task",
        date: "14-Feb-2024",
        isDone: false),
    Task(
        taskName: "Second Task",
        description: "Second task",
        date: "14-Feb-2024",
        isDone: false),
    Task(
        taskName: "Third Task",
        description: "Third task",
        date: "14-Feb-2024",
        isDone: false),
    Task(
        taskName: "Fourth Task",
        description: "Fourth task",
        date: "14-Feb-2024",
        isDone: false),
  ];

  TextEditingController tasknameController = TextEditingController();
  TextEditingController desnameController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  bool delete = false;
  bool isEdit = false;
  bool isTap = false;
  bool isLoginDone = false;

  TextEditingController userName = TextEditingController();
  TextEditingController passWord = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void addValues() {
    setState(() {
      if (tasknameController.text != "" &&
          desnameController.text != "" &&
          dateController.text != "") {
        tasks.add(Task(
            date: dateController.text,
            taskName: tasknameController.text,
            description: desnameController.text,
            isDone: false));

        tasknameController.text = "";
        desnameController.text = "";
        dateController.text = "";
      }
    });
  }

  void showErrorBottomSheetModalSubmit() {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40), topRight: Radius.circular(40))),
        context: context,
        builder: (BuildContext context) {
          return SizedBox(
            height: 200,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Center(
                    child: Text("Please fill the fields",
                        style: GoogleFonts.quicksand(
                            fontSize: 28, fontWeight: FontWeight.w600))),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(
                      HexColor("#6F51FF"),
                    ),
                    fixedSize: const MaterialStatePropertyAll(
                      Size(175, 20),
                    ),
                  ),
                  child: const Text(
                    "OK",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  void deleteTask(int index) {
    tasks.removeAt(index);
    setState(() {});
  }

  void addEditedValues(int index) {
    setState(() {
      tasks[index].taskName = tasknameController.text;

      tasks[index].date = dateController.text;

      tasks[index].description = desnameController.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isLogin) {
      return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            tasknameController.text = "";
            desnameController.text = "";
            dateController.text = "";

            showModalBottomSheet(
              isScrollControlled: true,
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40))),
              context: context,
              builder: (BuildContext context) => Container(
                // padding: const EdgeInsets.fromLTRB(20, 0, 20, 15),
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                padding: MediaQuery.of(context).viewInsets,
                child: Column(
                    // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Create Task",
                        style: GoogleFonts.quicksand(
                            fontSize: 28, fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Title",
                            style: GoogleFonts.quicksand(
                                fontSize: 13.75,
                                fontWeight: FontWeight.w400,
                                color: HexColor("#6F51FF")),
                          ),
                          TextField(
                            scrollPadding: const EdgeInsets.all(5),
                            controller: tasknameController,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(20),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: HexColor("#6F51FF"),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Description",
                            style: GoogleFonts.quicksand(
                                fontSize: 13.75,
                                fontWeight: FontWeight.w400,
                                color: HexColor("#6F51FF")),
                          ),
                          TextField(
                            spellCheckConfiguration:
                                const SpellCheckConfiguration(),
                            controller: desnameController,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(20),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: HexColor("#6F51FF"),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Date",
                            style: GoogleFonts.quicksand(
                                fontSize: 13.75,
                                fontWeight: FontWeight.w400,
                                color: HexColor("#6F51FF")),
                          ),
                          TextField(
                            onTap: () async {
                              DateTime? pickedDate = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(2024),
                                  lastDate: DateTime(2025));

                              String format =
                                  DateFormat.yMMMd().format(pickedDate!);

                              setState(() {
                                dateController.text = format;
                              });
                            },
                            readOnly: true,
                            scrollPadding: const EdgeInsets.all(5),
                            controller: dateController,
                            decoration: InputDecoration(
                              suffixIcon: Icon(Icons.calendar_month,
                                  color: HexColor("#6F51FF")),
                              contentPadding: const EdgeInsets.all(20),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: HexColor("#6F51FF"),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll(HexColor("#6F51FF"))),
                        onPressed: () {
                          if (tasknameController.text != "" &&
                              desnameController.text != "" &&
                              dateController.text != "") {
                            Navigator.of(context).pop();
                            addValues();
                          } else {
                            showErrorBottomSheetModalSubmit();
                          }
                        },
                        child: Container(
                          height: 60,
                          width: 300,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Center(
                            child: Text(
                              "Submit",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                    ]),
              ),
            );
          },
          backgroundColor: HexColor("#6F52FF"),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(40))),
          child: const Icon(
            Icons.add,
            color: Colors.white,
          ),
        ),
        backgroundColor: HexColor("#6F51FF"),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 100,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Good Morning",
                      style: GoogleFonts.quicksand(
                          fontSize: 30,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                    Text(
                      "Shree",
                      style: GoogleFonts.quicksand(
                          fontSize: 30,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                  ],
                )
              ],
            ),
            const SizedBox(
              height: 70,
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                // height: 200,
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40)),
                  color: HexColor("#D9D9D9"),
                ),

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Text(
                        "Create Task",
                        style: GoogleFonts.quicksand(
                            fontSize: 27,
                            fontWeight: FontWeight.w600,
                            color: Colors.black),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20)),
                          color: Colors.white,
                        ),
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            return Slidable(
                              endActionPane:
                                  ActionPane(motion: ScrollMotion(), children: [
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Container(
                                      height: 42,
                                      width: 42,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: HexColor("#6F51FF"),
                                      ),
                                      child: IconButton(
                                        onPressed: () {
                                          isEdit = true;
                                          if (isEdit) {
                                            tasknameController.text =
                                                tasks[index].taskName!;
                                            desnameController.text =
                                                tasks[index].description!;
                                            dateController.text =
                                                tasks[index].date!;
                                          }
                                          showModalBottomSheet(
                                            isScrollControlled: true,
                                            shape: const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.only(
                                                    topLeft:
                                                        Radius.circular(40),
                                                    topRight:
                                                        Radius.circular(40))),
                                            context: context,
                                            builder: (BuildContext context) =>
                                                Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  20, 0, 20, 0),
                                              padding: MediaQuery.of(context)
                                                  .viewInsets,
                                              child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    const SizedBox(
                                                      height: 15,
                                                    ),
                                                    Text(
                                                      "Edit Task",
                                                      style:
                                                          GoogleFonts.quicksand(
                                                              fontSize: 28,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                    ),
                                                    const SizedBox(
                                                      height: 15,
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          "Title",
                                                          style: GoogleFonts
                                                              .quicksand(
                                                                  fontSize:
                                                                      13.75,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  color: HexColor(
                                                                      "#6F51FF")),
                                                        ),
                                                        TextField(
                                                          scrollPadding:
                                                              const EdgeInsets
                                                                  .all(5),
                                                          controller:
                                                              tasknameController,
                                                          decoration:
                                                              InputDecoration(
                                                            contentPadding:
                                                                const EdgeInsets
                                                                    .all(20),
                                                            enabledBorder:
                                                                OutlineInputBorder(
                                                              borderSide:
                                                                  BorderSide(
                                                                color: HexColor(
                                                                    "#6F51FF"),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    const SizedBox(
                                                      height: 15,
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          "Description",
                                                          style: GoogleFonts
                                                              .quicksand(
                                                                  fontSize:
                                                                      13.75,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  color: HexColor(
                                                                      "#6F51FF")),
                                                        ),
                                                        TextField(
                                                          spellCheckConfiguration:
                                                              const SpellCheckConfiguration(),
                                                          controller:
                                                              desnameController,
                                                          decoration:
                                                              InputDecoration(
                                                            contentPadding:
                                                                const EdgeInsets
                                                                    .all(20),
                                                            enabledBorder:
                                                                OutlineInputBorder(
                                                              borderSide:
                                                                  BorderSide(
                                                                color: HexColor(
                                                                    "#6F51FF"),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    const SizedBox(
                                                      height: 15,
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          "Date",
                                                          style: GoogleFonts
                                                              .quicksand(
                                                                  fontSize:
                                                                      13.75,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  color: HexColor(
                                                                      "#6F51FF")),
                                                        ),
                                                        TextField(
                                                          onTap: () async {
                                                            DateTime?
                                                                pickedDate =
                                                                await showDatePicker(
                                                                    context:
                                                                        context,
                                                                    initialDate:
                                                                        DateTime
                                                                            .now(),
                                                                    firstDate:
                                                                        DateTime(
                                                                            2024),
                                                                    lastDate:
                                                                        DateTime(
                                                                            2025));

                                                            String format =
                                                                DateFormat
                                                                        .yMMMd()
                                                                    .format(
                                                                        pickedDate!);

                                                            setState(() {
                                                              dateController
                                                                      .text =
                                                                  format;
                                                            });
                                                          },
                                                          readOnly: true,
                                                          scrollPadding:
                                                              const EdgeInsets
                                                                  .all(5),
                                                          controller:
                                                              dateController,
                                                          decoration:
                                                              InputDecoration(
                                                            contentPadding:
                                                                const EdgeInsets
                                                                    .all(20),
                                                            enabledBorder:
                                                                OutlineInputBorder(
                                                              borderSide:
                                                                  BorderSide(
                                                                color: HexColor(
                                                                    "#6F51FF"),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    const SizedBox(
                                                      height: 25,
                                                    ),
                                                    ElevatedButton(
                                                      style: ButtonStyle(
                                                          backgroundColor:
                                                              MaterialStatePropertyAll(
                                                                  HexColor(
                                                                      "#6F51FF"))),
                                                      onPressed: () {
                                                        // if (tasknameController.text != "" ||
                                                        //     desnameController.text != "" ||
                                                        //     dateController.text != "") {
                                                        Navigator.of(context)
                                                            .pop();
                                                        addEditedValues(index);
                                                        // } else {
                                                        //   showErrorBottomSheetModalEdit();
                                                        // }
                                                      },
                                                      child: Container(
                                                        height: 60,
                                                        width: 300,
                                                        child: const Center(
                                                          child: Text(
                                                            "Submit",
                                                            style: TextStyle(
                                                                fontSize: 18,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    const SizedBox(
                                                      height: 25,
                                                    ),
                                                  ]),
                                            ),
                                          );
                                        },
                                        icon: const Icon(Icons.edit_outlined),
                                        color: Colors.white,
                                      ),
                                    ),
                                    Container(
                                      height: 42,
                                      width: 42,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: HexColor("#6F51FF"),
                                      ),
                                      child: IconButton(
                                        onPressed: () {
                                          showModalBottomSheet(
                                              shape:
                                                  const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(40),
                                                  topRight: Radius.circular(40),
                                                ),
                                              ),
                                              context: context,
                                              builder: (BuildContext context) {
                                                return SizedBox(
                                                    child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    const SizedBox(
                                                      height: 25,
                                                    ),
                                                    Text(
                                                      "Are you sure ?",
                                                      style:
                                                          GoogleFonts.quicksand(
                                                              fontSize: 28,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                    ),
                                                    const SizedBox(
                                                      height: 20,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        ElevatedButton(
                                                          style: ButtonStyle(
                                                            backgroundColor:
                                                                MaterialStatePropertyAll(
                                                              HexColor(
                                                                  "#6F51FF"),
                                                            ),
                                                            fixedSize:
                                                                const MaterialStatePropertyAll(
                                                              Size(175, 20),
                                                            ),
                                                          ),
                                                          onPressed: () {
                                                            delete = true;
                                                            deleteTask(index);
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                          child: const Text(
                                                            "Yes",
                                                            style: TextStyle(
                                                              fontSize: 18,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                          ),
                                                        ),
                                                        ElevatedButton(
                                                          style: ButtonStyle(
                                                            backgroundColor:
                                                                MaterialStatePropertyAll(
                                                              HexColor(
                                                                  "#6F51FF"),
                                                            ),
                                                            fixedSize:
                                                                const MaterialStatePropertyAll(
                                                              Size(175, 20),
                                                            ),
                                                          ),
                                                          onPressed: () {
                                                            setState(() {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            });
                                                          },
                                                          child: const Text(
                                                            "No",
                                                            style: TextStyle(
                                                              fontSize: 18,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                  ],
                                                ));
                                              });
                                        },
                                        icon: Icon(Icons.delete_outline),
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ]),
                              child: Container(
                                // height: 100,
                                padding:
                                    const EdgeInsets.fromLTRB(15, 10, 15, 10),
                                margin: const EdgeInsets.only(
                                    left: 0, top: 10, right: 0, bottom: 10),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        width: 0.02, color: Colors.black)),
                                child: Row(
                                  children: [
                                    Image.network(
                                      "https://media.istockphoto.com/id/1303877287/vector/paper-checklist-and-pencil-flat-pictogram.jpg?s=612x612&w=0&k=20&c=NoqPzn94VH2Pm7epxF8P5rCcScMEAiGQ8Hv_b2ZwRjY=",
                                      height: 52,
                                      width: 52,
                                    ),
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            tasks[index].taskName!,
                                            style: GoogleFonts.inter(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 18),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  tasks[index].description!,
                                                  style: GoogleFonts.inter(
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 14),
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    tasks[index].isDone =
                                                        !tasks[index].isDone;
                                                    setState(() {});
                                                  },
                                                  child: Container(
                                                    height: 16,
                                                    width: 16,
                                                    decoration: BoxDecoration(
                                                        color: tasks[index]
                                                                .isDone
                                                            ? Colors.greenAccent
                                                            : Colors.white,
                                                        borderRadius:
                                                            const BorderRadius
                                                                .all(
                                                          Radius.circular(25),
                                                        ),
                                                        border: tasks[index]
                                                                .isDone
                                                            ? Border.all(
                                                                color: Colors
                                                                    .white)
                                                            : Border.all(
                                                                color: Colors
                                                                    .black)),
                                                  ),
                                                )
                                              ]),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            tasks[index].date!,
                                            style: GoogleFonts.inter(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 14),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                          itemCount: tasks.length,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Login Page"),
          centerTitle: true,
          backgroundColor: HexColor("#6F51FF"),
        ),
        body: Padding(
            padding: EdgeInsets.all(8),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRv-Y_ULL1Ttrf_OeHJlIe2xNrCekDbKmFeFg&usqp=CAU",
                    height: 90,
                    width: 90,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: userName,
                    decoration: InputDecoration(
                      hintText: "Enter Username",
                      label: const Text("Username"),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      prefixIcon: Icon(Icons.person),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Please enter Username";
                      } else {
                        return null;
                      }
                    },
                    keyboardType: TextInputType.emailAddress,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: passWord,
                    obscureText: true,
                    obscuringCharacter: "*",
                    decoration: InputDecoration(
                        hintText: "Enter password",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        prefixIcon: const Icon(Icons.lock),
                        suffixIcon: const Icon(
                          Icons.remove_red_eye_outlined,
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Please Enter Password";
                      } else {
                        return null;
                      }
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                      onPressed: () {
                        bool loginValidated = _formKey.currentState!.validate();

                        if (loginValidated) {
                          isLogin = true;
                          setState(() {});
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text("Login Successful"),
                          ));
                        } else {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text("Login Failed"),
                          ));
                        }
                      },
                      child: const Text("Login"))
                ],
              ),
            )),
      );
    }
  }
}
