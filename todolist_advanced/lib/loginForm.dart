import 'package:flutter/material.dart';

bool isLogin = false;

bool getLogin() {
  return isLogin;
}

class Login extends StatefulWidget {
  @override
  State createState() => _login();
}

class _login extends State {
  TextEditingController userName = TextEditingController();
  TextEditingController passWord = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login Page"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Padding(
          padding: EdgeInsets.all(8),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRv-Y_ULL1Ttrf_OeHJlIe2xNrCekDbKmFeFg&usqp=CAU",
                  height: 90,
                  width: 90,
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: userName,
                  decoration: InputDecoration(
                    hintText: "Enter Username",
                    label: const Text("Username"),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    prefixIcon: Icon(Icons.person),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please enter Username";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: passWord,
                  obscureText: true,
                  obscuringCharacter: "*",
                  decoration: InputDecoration(
                      hintText: "Enter password",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      prefixIcon:const Icon(Icons.lock),
                      suffixIcon:const Icon(
                        Icons.remove_red_eye_outlined,
                      )),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Password";
                    } else {
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      bool loginValidated = _formKey.currentState!.validate();

                      if (loginValidated) {
                        isLogin = true;

                        ScaffoldMessenger.of(context)
                            .showSnackBar(const SnackBar(
                          content: Text("Login Successful"),
                        ));
                      } else {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(const SnackBar(
                          content: Text("Login Failed"),
                        ));
                      }
                    },
                    child: const Text("Login"))
              ],
            ),
          )),
    );
  }
}
