import 'package:flutter/material.dart';

class Task {
  String? taskName;
  String? description;
  String? date;
  int? id;
  bool isDone;

  Task({this.taskName, this.description, this.date,required this.isDone});
}
