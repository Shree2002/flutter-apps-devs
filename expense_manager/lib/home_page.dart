import 'package:expense_manager/my_drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'list.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  @override
  State createState() => _HomePage();
}

class _HomePage extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyDrawer(),
      floatingActionButton: GestureDetector(
        onTap: () {
          showModalBottomSheet(
            isScrollControlled: true,
            context: context,
            builder: (context) {
              return Container(
                // height: 400,
                // padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
                padding: MediaQuery.of(context).viewInsets,

                child: Container(
                  margin: const EdgeInsets.fromLTRB(15, 35, 15, 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Date",
                          style: GoogleFonts.poppins(
                              fontSize: 13, fontWeight: FontWeight.w400)),
                      const SizedBox(
                        height: 5,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                          color: Color.fromRGBO(191, 189, 189, 1),
                        ))),
                      ),
                      const SizedBox(height: 15),
                      Text("Amount",
                          style: GoogleFonts.poppins(
                              fontSize: 13, fontWeight: FontWeight.w400)),
                      const SizedBox(
                        height: 5,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                          color: Color.fromRGBO(191, 189, 189, 1),
                        ))),
                      ),
                      const SizedBox(height: 15),
                      Text("Category",
                          style: GoogleFonts.poppins(
                              fontSize: 13, fontWeight: FontWeight.w400)),
                      const SizedBox(
                        height: 5,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                          color: Color.fromRGBO(191, 189, 189, 1),
                        ))),
                      ),
                      const SizedBox(height: 15),
                      Text("Description",
                          style: GoogleFonts.poppins(
                              fontSize: 13, fontWeight: FontWeight.w400)),
                      const SizedBox(
                        height: 5,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                          color: Color.fromRGBO(191, 189, 189, 1),
                        ))),
                      ),
                      const SizedBox(height: 15),
                      Center(
                        child: Container(
                          height: 40,
                          width: 123,
                          decoration: const BoxDecoration(
                              color: Color.fromRGBO(14, 161, 125, 1),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(67))),
                          child: Center(
                            child: Text(
                              "Add",
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          );
        },
        child: Container(
          padding: const EdgeInsets.fromLTRB(0, 10, 5, 10),
          height: 71,
          width: 220,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(67)),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.25),
                  offset: Offset(0, 5),
                  blurRadius: 15)
            ],
            color: Colors.white,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(
                Icons.add_circle_sharp,
                size: 52,
                color: Color.fromRGBO(14, 161, 125, 1),
              ),
              const SizedBox(
                width: 5,
              ),
              Text(
                "Add Transaction",
                style: GoogleFonts.poppins(
                    fontSize: 16, fontWeight: FontWeight.w400),
              )
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      appBar: AppBar(
        title: Text(
          "June 2022",
          style: GoogleFonts.poppins(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: const Color.fromRGBO(33, 33, 33, 1),
          ),
        ),
        actions: [
          Image.asset(
            "assets/search.png",
            height: 40,
            width: 40,
          ),
          const SizedBox(
            width: 15,
          )
        ],
      ),
      body: ListView.builder(
        itemCount: 5,
        itemBuilder: (context, index) {
          return Container(
            width: double.infinity,
            // height: 75,
            margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            padding: const EdgeInsets.fromLTRB(15, 0, 10, 0),
            decoration: const BoxDecoration(
                border: Border(
                    bottom:
                        BorderSide(color: Color.fromRGBO(206, 206, 206, 1)))),
            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset(list[index]["img"]),
                    const SizedBox(
                      width: 8,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              list[index]["title"],
                              style: GoogleFonts.poppins(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ),
                            ),
                            const SizedBox(width: 180),
                            Row(
                              children: [
                                const Icon(
                                  Icons.remove_circle_rounded,
                                  color: Colors.red,
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "${list[index]["price"]}",
                                  style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 15,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                        Text(
                          list[index]["description"],
                          style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w400,
                            fontSize: 15,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        list[index]["date"],
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 10,
                          color: const Color.fromRGBO(0, 0, 0, 0.6),
                        ),
                      ),
                      Text(
                        " | ",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 10,
                          color: const Color.fromRGBO(0, 0, 0, 0.6),
                        ),
                      ),
                      Text(
                        list[index]["time"],
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 10,
                          color: const Color.fromRGBO(0, 0, 0, 0.6),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
