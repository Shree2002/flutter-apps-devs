import 'package:expense_manager/category.dart';
import 'package:expense_manager/graph_page.dart';
import 'package:expense_manager/home_page.dart';
import 'package:expense_manager/trash_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyDrawer extends StatefulWidget {
  @override
  State createState() => _MyDrawer();
}

class _MyDrawer extends State {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      shape:
          const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.zero)),
      child: Padding(
        padding: const EdgeInsets.only(left: 35),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 45,
            ),
            Text("Expense Manager",
                style: GoogleFonts.poppins(
                    fontSize: 16, fontWeight: FontWeight.w600)),
            Text(
              "Saves All Your Transactions",
              style: GoogleFonts.poppins(
                fontSize: 10,
                fontWeight: FontWeight.w400,
                color: const Color.fromRGBO(0, 0, 0, 0.5),
              ),
            ),
            const SizedBox(
              height: 05,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => const GraphPage()));
              },
              child: Container(
                height: 40,
                width: 184,
                decoration:
                    const BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0)),
                child: Row(
                  children: [
                    Image.asset("assets/graph.png"),
                    const SizedBox(
                      width: 10,
                    ),
                    Text("Graph",
                        style: GoogleFonts.poppins(
                            fontSize: 16, fontWeight: FontWeight.w500))
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => const HomePage()));
              },
              child: Container(
                height: 40,
                width: 184,
                decoration:
                    const BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0)),
                child: Row(
                  children: [
                    Image.asset("assets/Transaction.png"),
                    const SizedBox(
                      width: 10,
                    ),
                    Text("Transaction",
                        style: GoogleFonts.poppins(
                            fontSize: 16, fontWeight: FontWeight.w500))
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => const Category()));
              },
              child: Container(
                height: 40,
                width: 184,
                decoration:
                    const BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0)),
                child: Row(
                  children: [
                    Image.asset("assets/category.png"),
                    const SizedBox(
                      width: 10,
                    ),
                    Text("Category",
                        style: GoogleFonts.poppins(
                            fontSize: 16, fontWeight: FontWeight.w500))
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => const TrashPage()));
              },
              child: Container(
                height: 40,
                width: 184,
                decoration:
                    const BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0)),
                child: Row(
                  children: [
                    Image.asset("assets/trash.png"),
                    const SizedBox(
                      width: 10,
                    ),
                    Text("Trash",
                        style: GoogleFonts.poppins(
                            fontSize: 16, fontWeight: FontWeight.w500))
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                height: 40,
                width: 184,
                decoration:
                    const BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0)),
                child: Row(
                  children: [
                    Image.asset("assets/aboutus.png"),
                    const SizedBox(
                      width: 10,
                    ),
                    Text("About Us",
                        style: GoogleFonts.poppins(
                            fontSize: 16, fontWeight: FontWeight.w500))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
