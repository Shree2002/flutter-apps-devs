import "package:flutter/material.dart";
import 'package:google_fonts/google_fonts.dart';
import 'screen1.dart';

class AssignmentApp extends StatefulWidget {
  const AssignmentApp({super.key});

  @override
  State<AssignmentApp> createState() => _AssignmentAppState();
}

class _AssignmentAppState extends State<AssignmentApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ElevatedButton(
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => Screen1()));
            },
            child: Center(
              child: Container(
                decoration: const BoxDecoration(),
                child: Text(
                  "Screen 1",
                  style: GoogleFonts.poppins(fontSize: 14),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
