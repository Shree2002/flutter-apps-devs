import 'package:flutter/material.dart';

class ProviderClass extends ChangeNotifier {
  int? price;

  ProviderClass({this.price});

  void changeData(int price) {
    this.price = price;
    notifyListeners();
  }
}
