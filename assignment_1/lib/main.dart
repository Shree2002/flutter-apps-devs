import 'package:flutter/material.dart';
import 'UI/home_page.dart';
import 'package:provider/provider.dart';
import 'provider class/provider_class.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) {
          return ProviderClass(price: 800);
        },
        child: const MaterialApp(
          debugShowCheckedModeBanner: false,
          home: AssignmentApp(),
        ));
  }
}
