import "package:flutter/material.dart";

void main() {
  runApp(myApp());
}

class myApp extends StatelessWidget {
  const myApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: QuizApp(),
    );
  }
}

class QuizApp extends StatefulWidget {
  @override
  State createState() => _quizappstate();
}

class _quizappstate extends State {
  List<Map> allQuestions = [
    {
      "q": "Founder of lenskart?",
      "o": ["Piyush", "Aman", "Namita", "Vineetha"],
      "a": 0
    },
    {
      "q": "Founder of boat?",
      "o": ["Piyush", "Aman", "Namita", "Vineetha"],
      "a": 1
    },
    {
      "q": "CEO of Emcure?",
      "o": ["Piyush", "Aman", "Namita", "Vineetha"],
      "a": 2,
    },
    {
      "q": "Founder of Sugar?",
      "o": ["Piyush", "Aman", "Namita", "Vineetha"],
      "a": 3
    },
    {
      "q": "Founder of BharatPe?",
      "o": ["Piyush", "Aman", "Ashneer", "Vineetha"],
      "a": 2
    }
  ];

  bool questionScreen = true;
  int questionIndex = 0;
  int selectedINdex = -1;
  bool pageSelection = false;

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }

  MaterialStateProperty<Color?>? answerColor(int buttonIndex) {
    if (selectedINdex != -1) {
      if (allQuestions[questionIndex]["a"] == buttonIndex && !pageSelection) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (buttonIndex == selectedINdex) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return null;
      }
    }
    return null;
  }

  int correctAnswers = 0;

  Scaffold isQuestionScreen() {
    if (questionScreen == true) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("QuizApp",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontFamily: AutofillHints.addressState,
                fontStyle: FontStyle.italic,
              )),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const SizedBox(
                height: 30,
              ),
              const Text("Question"),
              const SizedBox(
                height: 10,
              ),
              Text("${questionIndex + 1}/${allQuestions.length}"),
              const SizedBox(
                height: 10,
              ),
              Text(
                allQuestions[questionIndex]["q"],
                style: const TextStyle(
                  fontSize: 25,
                ),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    if (selectedINdex == -1) selectedINdex = 0;
                  });
                },
                style: ButtonStyle(
                    backgroundColor: answerColor(0),
                    fixedSize: const MaterialStatePropertyAll(Size(200, 50))),
                child: Text(allQuestions[questionIndex]["o"][0]),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    if (selectedINdex == -1) selectedINdex = 1;
                  });
                },
                style: ButtonStyle(
                  backgroundColor: answerColor(1),
                  fixedSize: const MaterialStatePropertyAll(Size(200, 50)),
                ),
                child: Text(allQuestions[questionIndex]["o"][1]),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    if (selectedINdex == -1) selectedINdex = 2;
                  });
                },
                style: ButtonStyle(
                  backgroundColor: answerColor(2),
                  fixedSize: MaterialStatePropertyAll(Size(200, 50)),
                ),
                child: Text(allQuestions[questionIndex]["o"][2]),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    if (selectedINdex == -1) selectedINdex = 3;
                  });
                },
                style: ButtonStyle(
                  backgroundColor: answerColor(3),
                  fixedSize: const MaterialStatePropertyAll(Size(200, 50)),
                ),
                child: Text(allQuestions[questionIndex]["o"][3]),
              )
            ],
          ),
        ),
        floatingActionButton: BackButton(
          color: Colors.blue,
          onPressed: () {
            setState(() {
              if (selectedINdex != -1) {
                if (allQuestions[questionIndex]["a"] == selectedINdex) {
                  correctAnswers++;
                }
                selectedINdex = -1;
                if (questionIndex < 4) {
                  questionIndex += 1;
                } else if (questionIndex == 4) {
                  questionScreen = false;
                } else {
                  questionIndex = 4;
                }
              }
            });
          },
        ),
      );
    } else {
      return Scaffold(
          appBar: AppBar(
            title: const Text(
              "QuizApp",
              style: TextStyle(
                  // fontSize: BouncingScrollSimulation.maxSpringTransferVelocity,
                  fontWeight: FontWeight.w800),
            ),
            centerTitle: true,
          ),
          body: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                  "https://c8.alamy.com/comp/2H3HTYN/congratulation-letter-with-trophy-cup-in-gold-color-winner-trophy-icon-symbol-in-flat-style-vector-illustration-eps8-eps10-2H3HTYN.jpg"),
              const SizedBox(
                height: 30,
              ),
              const Text(
                "Your Score is",
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                "$correctAnswers/${allQuestions.length}",
                style: const TextStyle(fontSize: 25),
              ),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    selectedINdex = -1;
                    questionIndex = 0;
                    correctAnswers = 0;
                    questionScreen = true;
                  });
                },
                child: const Text(
                  "Start Again",
                  style: TextStyle(fontSize: 15),
                ),
              )
            ],
          ));
    }
  }
}
